#!/bin/sh

awk -F'[<>]' ' {
        for ( i = 1; i <= NF; i++ )
        {
                if ( $i == "span" )
                        f = 1
                if ( $i == "/span" )
                        f = 0
                if ( f && $i != "span" )
                        print $i
        }
} ' 

awk '{print ($0 ~ /corsivo light/)?NR "yes":NR "no"}' Articolo_91-100_cambio\ font.html #THIS WORKS