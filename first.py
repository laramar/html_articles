#! ./bin/python3
import re
from bs4 import BeautifulSoup as bs4
import os


def hyperlink(): #all of this is done bypassing beautifulsoup, using the "soup", but with pure python coding to search and replace. Beautifulsoup didn't work well
	text_soup = str(soup)
	try:
		links = re.findall(r"(http|ftp|https)(:\/\/)([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])", text_soup)
		with open(doi+"_link", "w") as link_html:
			for l in links:
				good_link = ''.join(l)
				print(good_link)
				if good_link == "http://www.w3.org/1999/xhtml":
					pass
				else:
					link_html.write(good_link+"\n")

	except:
		pass
	'''
	for text in soup.find_all(text=True):
		for link in good_links:
			if link == text:
				print(text)
				a_tag = soup.new_tag("a", href=link)
				a_tag.string = link
				print(a_tag)
				new_text = text.replace(link, str(a_tag))
				print(new_text)'''
		#soup.replace_with(a_tag)
		#if link in soup.find("body").text: #https://stackoverflow.com/questions/46739521/python-beautifulsoup-text-none-type
	'''
	print("Good link", link)
	good_link = "<a href="+link+">"+link+"</a>"
	print(good_link)
	fixed_text = soup.text.replace(link, good_link)
	soup.replace_with(fixed_text)
	print(soup)'''
		#a_tag = soup.new_tag("a", href=link)
		#a_tag.string = link
		#print(a_tag)
	#print(soup)



def footNote(): #Note a pié di pagina
	try:
		div_ft = soup.find(class_="_idGenObjectLayout-2")
		div_ft.attrs["class"] = "footNotes"
		div_ft.insert_before(soup.new_tag("hr"))
		ft_note = soup.find_all(class_="_idFootnote") 
		for div in ft_note:
			if "apice" in div.span.attrs["class"]:
				div.span.name="sup"
				del div.sup.attrs["class"]
			div.a["href"] = re.sub(r'^[^#]+', "", div.a["href"])
	except:
		pass

def subCorsivo(span):
	try:
		if "corsivo" in span.attrs["class"]:
			span.name="em"
			del span.attrs["class"]
		elif "light_italic" in span.attrs["class"]:
			span.name="em"
			del span.attrs["class"]
		elif "notelight_italic" in span.attrs["class"]:
			span.name="em"
			del span.attrs["class"]
		elif "Italic" in span.attrs["class"]:
			span.name="em"
			del span.attrs["class"]
	except:
		pass

def subFootnote(span): #note nel testo
	try:
		if "apice" in span.attrs["class"]:
			span.name="sup"
			del span.attrs["class"]
		span.a["href"] = re.sub(r'^[^#]+', "", span.a["href"])
	except:
		pass

def subSemibold(span):
	try:
		if "semibold1" in span.attrs["class"]:
			span.name="b"
			del span.attrs["class"]
		elif "semibold2" in span.attrs["class"]:
			span.name="b"
			del span.attrs["class"]
		elif "Bold"  in span.attrs["class"]:
			span.name="b"
			del span.attrs["class"]
	except:
		pass


def cleanGen(span): #pulizie generiche
	GenColor= span.find(class_= "_idGenColorInherit")
	if GenColor:
		GenColor["class"].remove('_idGenColorInherit') #https://stackoverflow.com/questions/26507463/using-beautifulsoup-how-do-i-remove-a-single-class-from-an-element-with-multiple
		#print(GenColor)
	

	try:
		classes = span.attrs["class"]
		for i in classes:
			if "CharOverride" in i:
				span["class"].remove(i)
		if span.attrs["class"] == []:
			span.unwrap()
	except:
		pass

	#print("Clean generic done")


def imageCreate():
	try:
		divsImage = soup.find_all(class_="_idGenObjectLayout-1")
		for div in divsImage:
			divs = div.find_all("div")
			for dvs in divs:
				dvs.unwrap() #remove all useless divs: parents of image
			div.unwrap() #remove the first parent div of image
		imgs = soup.find_all("img")
		for img in imgs:
			del img.attrs["class"]
	except:
		pass

def titoli():
	titoli = soup.find_all("p")
	for tit in titoli:
		try:
			if "titoletti" in tit.attrs["class"]:
				tit.name="h2"
				del tit.attrs["class"]
			elif "titolettinote" in tit.attrs["class"]:
				tit.name="h2"
				del tit.attrs["class"]
		except:
			pass

def citazione():
	citazioni = soup.find_all("p")
	for cit in citazioni:
		try:
			if "citazione" in cit.attrs["class"]:
				cit.name="blockquote"
				del cit.attrs["class"]
			elif "citazione-1a-riga" in cit.attrs["class"]:
				cit.name="blockquote"
				del cit.attrs["class"]
		except:
			pass

def testo():
	testo = soup.find_all("p", class_="Testo")
	for t in testo:
		#if "Testo" in t.attrs["class"]: #clean also p=testo
		del t.attrs["class"]
	testo_note = soup.find_all("p", class_="testo_note")
	for t_n in testo_note:
		#if "Testo" in t.attrs["class"]: #clean also p=testo
		del t_n.attrs["class"]

def cornice():
	cornice = soup.find_all("div", class_="Cornice-di-testo-di-base")
	for c in cornice:
		c.unwrap()


current_dir = os.chdir("Clionet_dic")
no_doi = 0

for file in os.listdir(current_dir):
	if file.endswith(".html"):
		print(file)
		
		with open(file, "r") as html_doc:
			soup = bs4(html_doc, 'lxml')


			span = soup.find_all("span")
			if soup.find("span", class_=re.compile("^doi$", re.I)):
				doi = soup.find("span", class_=re.compile("^doi$", re.I)).text.replace("Doi: ", "").replace(".", "-").replace("/", "_")
			elif soup.find("p", class_=re.compile("^doi$", re.I)):
				doi = soup.find("p", class_=re.compile("^doi$", re.I)).text.replace("Doi: ", "").replace(".", "-").replace("/", "_")
			else:
				no_doi += 1
				doi = "no_doi"+str(no_doi)

			hyperlink()
			imageCreate()
			titoli()
			footNote()
			citazione()
			testo()
			cornice()

			for s in span:
					subSemibold(s)
					subCorsivo(s)
					subFootnote(s)
					cleanGen(s)
			if soup.find("span", class_="Autore"):
				autore = soup.find("span", class_="Autore").text
			elif soup.find("p", class_="Autore"):
				autore = soup.find("p", class_="Autore").text
			else:
				pass

			string_soup = str(soup)
		try:
			with open(autore, "w") as autore_bio:
				bio_it = soup.find("p", class_="Biografia_italiano").text
				bio_eng = soup.find("p", class_="Biografia_inglese").text
				autore_bio.write(bio_it+"\n<em>"+bio_eng+"</em>")
		except:
			pass		   

		with open(doi+".html", "w") as new_html:
			new_html.write(string_soup) 


#print(soup)








#semibold italic
#italic bastone
#titoletticorsivi

#Hyperlink regex: (http|ftp|https):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])

#class=Titolo
#class=Titoletto