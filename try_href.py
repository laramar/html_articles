import re
import requests
from bs4 import BeautifulSoup as bs4


with open("esempio-10-30682_clionet2206a.html", "r") as html_doc:
    soup = bs4(html_doc, 'lxml')

item = soup.select_one('p:has(a)')
text = item.text

for tag in item.select('a:not([id])'):
    href = tag['href']
    tag.attrs = {'href': href}
    text = re.sub(href, str(tag), text)

    text = re.sub(item.a.text, '', text).strip()
    print(text)

"""
r = requests.get('https://rivista.clionet.it/vol5/giorgi-zoppi-la-ricerca-indire-tra-uso-didattico-del-patrimonio-storico-culturale-e-promozione-delle-buone-pratiche/')
soup = bs(r.text, 'lxml')
item = soup.select_one('p:has(a[id="ft-note-16"])')
text = item.text

for tag in item.select('a:not([id])'):
    href = tag['href']
    tag.attrs = {'href': href}
    text = re.sub(href, str(tag), text)

text = re.sub(item.a.text, '', text).strip()
print(text)"""